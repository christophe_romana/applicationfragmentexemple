package com.christopheromana.applicationfragmentexemple.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.christopheromana.applicationfragmentexemple.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Resultats extends Fragment {


    public Resultats() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_resultats, container, false);
    }

}
