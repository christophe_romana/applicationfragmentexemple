package com.christopheromana.applicationfragmentexemple.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.christopheromana.applicationfragmentexemple.MainActivity;
import com.christopheromana.applicationfragmentexemple.R;
import com.christopheromana.applicationfragmentexemple.adapter.MyAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment {
    RecyclerView recycler;
    private String[] myDataset = {"Fruit", "Legume",
            " dfsdf", "Poisson", "Viande", "Fruit", "Legume",
            "Poisson", "Viande", "Fruit", "Legume", "Poisson",
            "Viande", "Fruit", "Legume", "Poisson", "Viande"};
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public MenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View me = inflater.inflate(R.layout.fragment_menu, container, false);
        recycler = (RecyclerView) me.findViewById(R.id.menu);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recycler.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());

        recycler.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(myDataset, (MainActivity) getActivity());
        recycler.setAdapter(mAdapter);
        return me;
    }

}
