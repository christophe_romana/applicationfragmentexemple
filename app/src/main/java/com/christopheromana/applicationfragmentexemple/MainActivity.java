package com.christopheromana.applicationfragmentexemple;

import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.christopheromana.applicationfragmentexemple.fragments.Home;
import com.christopheromana.applicationfragmentexemple.fragments.MenuFragment;

public class MainActivity extends AppCompatActivity {
    DrawerLayout menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null) {
            return;
        }
         menu = (DrawerLayout) findViewById(R.id.drawer_layout);
        findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new Home());

            }
        });
        findViewById(R.id.openMenu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.openDrawer(GravityCompat.START);
            }
        });
        MenuFragment firstFragment = new MenuFragment();
        getSupportFragmentManager().beginTransaction()
        .replace(R.id.container, firstFragment).commit();
    }

    public void changeFragment(Fragment f){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, f)
                .addToBackStack("retour")
                .commit();
        menu.closeDrawer(GravityCompat.START);
    }

}




